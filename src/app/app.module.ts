import {NgModule} from '@angular/core';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
// components
import { AppComponent } from './app.component';

// Main Routes
import { AppRoutingModule } from './app-routing.module';

// Toast Module
import {ToastrModule} from 'ngx-toastr';

// Modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialDesignerModule } from './material-designer/material-designer.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeModule } from './home/home.module';
import { AuthModule } from './auth/auth.module';
import { ClientModule } from './home/client/client.module';
import { UtilsModule } from 'app/utils/utils.module';
import {NgxLoadingModule} from "ngx-loading";

@NgModule({
  declarations: [
    AppComponent,
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        MaterialDesignerModule,
        FormsModule,
        ReactiveFormsModule,
        HomeModule,
        AuthModule,
        HammerModule,
        ClientModule,
        InlineSVGModule.forRoot(),
        ToastrModule.forRoot({
            positionClass: 'toast-top-right'
        }),
        UtilsModule,
        NgxLoadingModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
