import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {DataRecoverPassword} from "../types/data-recover-password";

@Injectable({
  providedIn: 'root'
})
export class DataRecoverPasswordService {

  constructor(
    private http: HttpClient
  ) { }

  getDataRecoverPassword() {
    return this.http.get<DataRecoverPassword>('../../../assets/data/recover-password.json').toPromise()
  }
}
