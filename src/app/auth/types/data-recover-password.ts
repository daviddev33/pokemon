export interface DataRecoverPassword {
  paragraphSectionEmail: string;
  subTitleSectionPin: string;
  paragraphSectionPin: string;
  time:Time;
}

interface Time {
  minutes: string;
  seconds: string;
}
