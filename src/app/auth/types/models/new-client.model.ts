export class Client {
  constructor(
    public name: string,
    public lastName: string,
    public email: string,
    public docType: string,
    public doc: number,
    public phone: number,
  ) {

  }
}

export enum listDocuments {
  CC,
  CE,
  TI,
  RC,
}
