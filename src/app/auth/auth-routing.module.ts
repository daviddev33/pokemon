import { NgModule } from '@angular/core';
import { RegisterClientComponent } from './components/register-client/register-client.component';
import { RegisterLawyerComponent } from './components/register-lawyer/register-lawyer.component';
import {RouterModule, Routes} from '@angular/router';
import { LoginClientComponent } from './components/login-client/login-client.component';
import {RecoverPasswordComponent} from "./components/recover-password/recover-password.component";
import {LoginLawyerComponent} from "./components/login-lawyer/login-lawyer.component";

const routes: Routes = [
  { path: '',
  children: [
    { path: 'registro-cliente', component: RegisterClientComponent },
    { path: 'registro-abogado', component: RegisterLawyerComponent},
    { path: 'iniciar-sesion-cliente', component: LoginClientComponent},
    { path: 'recuperar-contrasena', component: RecoverPasswordComponent},
    { path: 'iniciar-sesion-abogado', component:LoginLawyerComponent }
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AuthRoutingModule { }
