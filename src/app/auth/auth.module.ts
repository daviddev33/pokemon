import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Modules
import { MaterialDesignerModule } from 'app/material-designer/material-designer.module';
import { AuthRoutingModule } from './auth-routing.module';
// Components
import { RegisterClientComponent } from './components/register-client/register-client.component';
import { RegisterLawyerComponent } from './components/register-lawyer/register-lawyer.component';
import {
  TermsConditionsComponent,
  TermsConditionsDialogComponent
} from './components/terms-conditions/terms-conditions.component';
import { LoginClientComponent } from './components/login-client/login-client.component';
import {MatButtonModule} from '@angular/material/button';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { LoginLawyerComponent } from './components/login-lawyer/login-lawyer.component';

@NgModule({
  declarations: [
    RegisterClientComponent,
    RegisterLawyerComponent,
    TermsConditionsComponent,
    TermsConditionsDialogComponent,
    LoginClientComponent,
    RecoverPasswordComponent,
    LoginLawyerComponent
  ],
    imports: [
        CommonModule,
        RouterModule,
        MaterialDesignerModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        AuthRoutingModule,
        MatButtonModule
    ],
  exports: [
    RegisterClientComponent,
    RegisterLawyerComponent,
    TermsConditionsComponent,
    TermsConditionsDialogComponent
  ],
})
export class AuthModule {
}
