import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertsService} from "@services/alerts/alerts.service";

@Component({
  selector: 'app-login-lawyer',
  templateUrl: './login-lawyer.component.html',
  styleUrls: ['./login-lawyer.component.scss']
})
export class LoginLawyerComponent implements OnInit {

  public formLoginLawyer: FormGroup;
  private regexEmail = '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$';

  constructor(
    private formBuilder: FormBuilder,
    private alert: AlertsService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  buildForm() {
    this.formLoginLawyer = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.regexEmail)]],
      password: ['', [Validators.required]]
    });
  }

  get invalidEmailEntry() {
    const field = this.formLoginLawyer.get('email');
    return field.touched && field.invalid;
  }

  get invalidPasswordEntry() {
    const field = this.formLoginLawyer.get('password');
    return field.touched && field.invalid;
  }

  logInLawyer() {
    if (this.formLoginLawyer.invalid) {
      this.formLoginLawyer.markAllAsTouched();
      if (Boolean(this.invalidPasswordEntry && this.invalidEmailEntry)) {
        this.alert.showError('Por favor ingrese todos los campos', '');
      } else if (Boolean(this.invalidPasswordEntry)) {
        this.alert.showError('Por favor ingrese una contraseña', '');
        return;
      } else if (Boolean(this.invalidEmailEntry)) {
        this.alert.showError('formato de correo electrónico erróneo', '');
        return;
      }
      return;
    }
  }

}
