import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

// Services
import {AlertsService} from '@services/alerts/alerts.service';

@Component({
  selector: 'app-login-client',
  templateUrl: './login-client.component.html',
  styleUrls: ['./login-client.component.scss']
})
export class LoginClientComponent implements OnInit {
  public formLogin: FormGroup;
  private regexEmail = '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$';
  public scroll: HTMLCollection;

  constructor(
    private formBuilder: FormBuilder,
    private alert: AlertsService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.scroll = document.getElementsByClassName('background');
    this.scroll[0]?.scroll(0, 0);
  }

  buildForm() {
    this.formLogin = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.regexEmail)]],
      password: ['', [Validators.required]]
    });
  }

  logIn() {
    if (this.formLogin.invalid) {
      this.formLogin.markAllAsTouched();
      if (Boolean(this.invalidPasswordEntry && this.invalidEmailEntry)) {
        this.alert.showError('Por favor ingrese todos los campos', '');
      } else if (Boolean(this.invalidPasswordEntry)) {
        this.alert.showError('Por favor ingrese una contraseña', '');
        return;
      } else if (Boolean(this.invalidEmailEntry)) {
        this.alert.showError('formato de correo electrónico erróneo');
        return;
      }
      return;
    }
  }

  /**
   * Validate that the email input is valid
   */
  get invalidEmailEntry() {
    const field = this.formLogin.get('email');
    return field.touched && field.invalid;
  }

  /**
   * Validate that the password input is valid
   */
  get invalidPasswordEntry() {
    const field = this.formLogin.get('password');
    return field.touched && field.invalid;
  }
}
