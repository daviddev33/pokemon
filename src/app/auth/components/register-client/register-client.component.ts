import {Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

// Services
import {AlertsService} from '@services/alerts/alerts.service';

// Models
import {Client, listDocuments} from 'app/auth/types/models/new-client.model';

@Component({
  selector: 'app-register-client',
  templateUrl: './register-client.component.html',
  styleUrls: ['./register-client.component.scss']
})
export class RegisterClientComponent implements OnInit {
  public visible = true;
  public formClient: FormGroup;
  private regexEmail = '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$';
  private client: Client = {name: '', lastName: '', email: '', docType: '', doc: 0, phone: 0};
  public top:HTMLCollection;
  public documentTP = [
    {value: listDocuments.CC, viewValue: 'CC'},
    {value: listDocuments.CE, viewValue: 'CE'},
    {value: listDocuments.TI, viewValue: 'TI'},
    {value: listDocuments.RC, viewValue: 'RC'},
  ];

  constructor(
    private formBuilder: FormBuilder,
    private alert: AlertsService,
  ) {
    this.formClient = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern(this.regexEmail)]],
      documentType: ['', [Validators.required]],
      document: ['', [Validators.required]],
      phone: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.top = document.getElementsByClassName('background');
    this.top[0]?.scroll(0,0);
  }

  changeView() {
    this.visible = !this.visible ;
  }

  signUp() {
    if (this.formClient.valid) {
      const name = this.formClient.get('name').value;
      const lastName = this.formClient.get('lastName').value;
      const email = this.formClient.get('email').value;
      const documentType = this.formClient.get('documentType').value;
      const document = this.formClient.get('document').value;
      const phone = this.formClient.get('phone').value;
      this.client = this.createNewClient(name, lastName, email, documentType, document, phone);
      this.formClient.reset();
      this.alert.showSuccess('Usuario creado satisfactoriamente', '');
    } else {
      this.formClient.markAllAsTouched();
      this.alert.showWarning('Por favor verifique datos', '');
    }
  }

  createNewClient(name: string, lastName: string, email: string, docType: string, doc: number, phone: number) {
    return new Client(name, lastName, email, docType, doc, phone);
  }

  get invalidNameEntry() {
    const field = this.formClient.get('name');
    return field.touched && field.invalid;
  }

  get invalidLastNameEntry() {
    const field = this.formClient.get('lastName');
    return field.touched && field.invalid;
  }

  get invalidEmailEntry() {
    const field = this.formClient.get('email');
    return field.touched && field.invalid;
  }

  get invalidDocumentTypeEntry() {
    const field = this.formClient.get('documentType');
    return field.touched && field.invalid;
  }

  get invalidDocumentEntry() {
    const field = this.formClient.get('document');
    return field.touched && field.invalid;
  }

  get invalidPhoneEntry() {
    const field = this.formClient.get('phone');
    return field.touched && field.invalid;
  }
}
