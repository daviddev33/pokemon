import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsComponent implements OnInit {
  @Output() verification = new EventEmitter<string>();
  public isChecked = false;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  public confirmation(value: string) {
    this.verification.emit(value);
  }

  openDialog() {
    const valueDialog = this.dialog.open(TermsConditionsDialogComponent, {panelClass: 'container-terms'});
    valueDialog.afterClosed().subscribe(result => {
      if (result === 'on') {
        this.isChecked = true;
        this.confirmation(result);
      } else {
        this.isChecked = false;
      }
    });
  }
}

@Component({
  selector: 'app-terms-conditions-dialog',
  templateUrl: './terms-conditions-dialog.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsDialogComponent {
  public flag = 'on';

  constructor(public dialogRef: MatDialogRef<TermsConditionsDialogComponent>) {
  }
}

