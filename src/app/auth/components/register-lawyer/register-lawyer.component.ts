import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

// service
import {AlertsService} from '@services/alerts/alerts.service';

@Component({
  selector: 'app-register-lawyer',
  templateUrl: './register-lawyer.component.html',
  styleUrls: ['./register-lawyer.component.scss']
})
export class RegisterLawyerComponent implements OnInit {
  public isChecked = false;
  public formLawyer: FormGroup;
  public formBuffett: FormGroup;

  constructor(private fb: FormBuilder, private toast: AlertsService) {
    this.formLawyer = this.fb.group({
      name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]*$/)]],
      lastName: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]*$/), Validators.nullValidator]],
      typeId: ['', [Validators.required]],
      id: ['', [Validators.required, Validators.pattern(/[0-9+\- ]/)]],
      phone: ['', [Validators.required, Validators.pattern(/[0-9+\- ]/)]],
      cardId: ['', [Validators.required, Validators.pattern(/[0-9+\- ]/)]],
    });
    this.formBuffett = this.fb.group({
      socialReason: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]*$/)]],
      NIT: ['', [Validators.required, Validators.pattern(/[0-9+\-]/)]],
      officePhone: ['', [Validators.required, Validators.pattern(/[0-9+\- ]/)]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  public register() {
    if (this.formLawyer.valid || this.formBuffett.valid) {
      this.formBuffett.reset();
      this.formLawyer.reset();
    } else {
      this.formLawyer.markAllAsTouched();
      this.toast.showError('Ingrese todos los Campos Requeridos', '');
    }
  }
  get nameField() {
    const validator = this.formLawyer.get('name');
    return validator.touched && validator.invalid;
  }

  get lastNameField() {
    const validator = this.formLawyer.get('lastName');
    return validator.touched && validator.invalid;
  }

  get typeIdField() {
    const validator = this.formLawyer.get('typeId');
    return validator.touched && validator.invalid;
  }

  get idField() {
    const validator = this.formLawyer.get('id');
    return validator.touched && validator.invalid;
  }

  get phoneField() {
    const validator = this.formLawyer.get('phone');
    return validator.touched && validator.invalid;
  }

  get cardIdField() {
    const validator = this.formLawyer.get('cardId');
    return validator.touched && validator.invalid;
  }

  get socialReasonField() {
    const validator = this.formBuffett.get('socialReason');
    return validator.touched && validator.invalid;
  }

  get NITField() {
    const validator = this.formBuffett.get('NIT');
    return validator.touched && validator.invalid;
  }

  get officePhoneField() {
    const validator = this.formBuffett.get('officePhone');
    return validator.touched && validator.invalid;
  }

  get emailField() {
    const validator = this.formBuffett.get('email');
    return validator.touched && validator.invalid;
  }

  get addressField() {
    const validator = this.formBuffett.get('address');
    return validator.touched && validator.invalid;
  }

  get acceptTermsField() {
    const validator = this.formBuffett.get('acceptTerms');
    return validator.touched && validator.invalid;
  }

  public getAccept(data: string) {
    const confirmation = this.formLawyer.get('acceptTerms');
    return confirmation.setValue(data);
  }
}
