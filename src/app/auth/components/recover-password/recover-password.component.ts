import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataRecoverPasswordService} from "../../services/data-recover-password.service";
import {DataRecoverPassword} from "../../types/data-recover-password";
import {AlertsService} from "@services/alerts/alerts.service";
import {Router} from "@angular/router";
import {LoadingService} from "../../../shared/services/loading.service";

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
  @ViewChild('input1') input1: ElementRef;
  @ViewChild('input2') input2: ElementRef;
  @ViewChild('input3') input3: ElementRef;
  @ViewChild('input4') input4: ElementRef;
  public formEmail: FormGroup;
  public formPin: FormGroup;
  public data: DataRecoverPassword;
  public showMinutes: string;
  public showSeconds: string;
  public changeView: boolean = false;
  private isDisable: boolean = true;
  private regexEmail: string = '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$';

  constructor(
    private formBuilder: FormBuilder,
    private alert: AlertsService,
    private dataRecoverPassword: DataRecoverPasswordService,
    private route: Router,
    private loadingService: LoadingService,
  ) {
    this.buildForm();
  }

  async ngOnInit() {
    await this.getData();
  }

  /**
   * function for build forms
   */
  buildForm() {
    this.formEmail = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.regexEmail)]],
    });
    this.formPin = this.formBuilder.group({
      firstDigit: ['', [Validators.required, Validators.maxLength(1)]],
      secondDigit: ['', [Validators.required, Validators.maxLength(1)]],
      thirdDigit: ['', [Validators.required, Validators.maxLength(1)]],
      fourthDigit: ['', [Validators.required, Validators.maxLength(1)]],
    });
  }

  /**
   * Function to bring the information to be displayed in the retrieve password view
   */
  async getData() {
     this.loadingService.setLoading(true);
   try {
       this.data = await this.dataRecoverPassword.getDataRecoverPassword();
      this.showMinutes = this.data.time.minutes;
      this.showSeconds = this.data.time.seconds;
    } catch (err) {
      console.error(err);
      this.alert.showError('Ha ocurrido un error', '');
    }
    this.loadingService.setLoading(false);
  }

  /**
   * Function to capture the email if the input is valid
   */
  sendEmail() {
    if (Boolean(this.invalidForm(this.formEmail))) {
      this.alert.showWarning('Formato de correo electrónico erróneo', '');
      return;
    };
    const email = this.formEmail.get('email').value;
    this.changeInterface();
    this.timer();
  }

  /**
   * Function to capture the pin if the input is valid
   */
  sendUserPinToUs() {
    if (Boolean(this.invalidForm(this.formPin))) {
      this.alert.showWarning('Debe ingresar los 4 dígitos', '');
      return;
    }
    const valueForm = this.formPin.value;
    const pin = parseInt(`${valueForm.firstDigit}${valueForm.secondDigit}${valueForm.thirdDigit}${valueForm.fourthDigit}`);
    this.route.navigate(['home/cliente/autenticacion/iniciar-sesion-cliente']);
  }

  /**
   * Function to move focus from one input to another each time a character is typed
   */
  moveFocus() {
    let inputLength = this.input1.nativeElement.value.length;
    let inputMaxLength = this.input1.nativeElement.maxLength;
    if (inputLength === inputMaxLength) {
      this.input2.nativeElement.focus();
    };
    inputLength = this.input2.nativeElement.value.length;
    inputMaxLength = this.input2.nativeElement.maxLength;
    if (inputLength === inputMaxLength) {
      this.input3.nativeElement.focus();
    };
    inputLength = this.input3.nativeElement.value.length;
    inputMaxLength = this.input3.nativeElement.maxLength;
    if (inputLength === inputMaxLength) {
      this.input4.nativeElement.focus();
    };
  }

  /**
   * Resend the pin, reset the timer, and disable the resend button
   */
  reSendCodeToUser() {
    this.isDisable = !this.isDisable;
    this.timer();
  }

  /**
   * Validate if a form is invalid
   * @param form
   */
  invalidForm(form) {
    return form.touched && form.invalid;
  }

  /**
   * Function that allows the interface change
   */
  changeInterface() {
    this.changeView = !this.changeView;
  }

  /**
   * Countdown timer
   */
  timer() {
    /**
     * As we are going to operate only with minutes and seconds,
     * we assign any date with the desired minutes and seconds;
     * assign the interval to a variable and then delete it when the time is up;
     * subtract 1000 milliseconds from the current date, that is, one second,
     * until the time hits 00:00, finally remove the interval and enable the resend button
     */
    let date = new Date(`January 01, 2020 00:${this.data.time.minutes}:${this.data.time.seconds}`);
    let padLeft = n => "00".substring(0, "00".length - n.length) + n;
    let interval = setInterval(() => {
      let minutes = padLeft(date.getMinutes() + "");
      this.showMinutes = minutes;
      let seconds = padLeft(date.getSeconds() + "");
      this.showSeconds = seconds;
      date = new Date(date.getTime() - 1000);
      if (minutes == '00' && seconds == '00') {
        this.isDisable = !this.isDisable;
        clearInterval(interval);
      }
    }, 1000);
  }
}
