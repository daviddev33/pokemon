export interface Lawyer {
  id?: string;
  name: string;
  office?: string;
  score: number;
  experience: number;
  shortDescription?: string;
  description?: string;
  image: string;
  city?: string;
  specialty?: Specialty[];
  category?: string;
  phone?: string;
  cellphone?: string;
  webPage?: string;
  email?: string;
  date?: DateElement[];
}
export interface DateElement {
  hour: string;
}
interface Specialty {
  name: string;
}

export interface ContactPoint {
  rank?: number;
  value: string;
  sourceId: ContactPointType;
}

export enum ContactPointType {
  PHONE,
  EMAIL,
  WEB
}

export interface LawyerCardOptions {
  background: 'blue-bg'| 'white-bg';
  contactOptions: boolean;
  description: boolean;
  premiumMark: boolean;
  office?: boolean ;
  avatar?: boolean;
  hideInformation?: boolean;
}
