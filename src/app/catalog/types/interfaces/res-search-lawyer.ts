import { Lawyer } from "./index";

export interface ResponseSearchLawyer {
    lawyers: Lawyer[]
}