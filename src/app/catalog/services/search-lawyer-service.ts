import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseSearchLawyer } from '../types/interfaces/index';


@Injectable({
  providedIn: 'root'
})
export class LawyerCatalogService {

  constructor(
    private http: HttpClient,
  ) { }

  getLawyersCatalog():Promise<ResponseSearchLawyer>{
    return this.http.get<ResponseSearchLawyer>('../../../assets/data/lawyers-data.json').toPromise(); 
  }

}
