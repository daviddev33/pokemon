import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import { LawyerCatalogComponent } from './components/lawyer-catalog/lawyer-catalog.component';
import {LawyerFilterComponent} from "./components/lawyer-filter/lawyer-filter.component";
const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: LawyerCatalogComponent},
      {path: 'filtrar-abogado', component: LawyerFilterComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
