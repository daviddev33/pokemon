import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

//modules
import {CatalogRoutingModule} from './catalog-routing.module';
import {UtilsModule} from 'app/utils/utils.module';
import {MaterialDesignerModule} from 'app/material-designer/material-designer.module';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {SharedModule} from "../shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import {InlineSVGModule} from "ng-inline-svg";

//components
import {LawyerCatalogComponent} from './components/lawyer-catalog/lawyer-catalog.component';
import {LawyerFilterComponent} from './components/lawyer-filter/lawyer-filter.component';

@NgModule({
  declarations: [
    LawyerCatalogComponent,
    LawyerFilterComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    MaterialDesignerModule,
    MatButtonModule,
    MatIconModule,
    UtilsModule,
    ReactiveFormsModule,
    SharedModule,
    InlineSVGModule
  ]
})
export class CatalogModule {
}
