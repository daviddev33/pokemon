import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LawyerCatalogComponent } from './lawyer-catalog.component';

describe('LawyerCatalogComponent', () => {
  let component: LawyerCatalogComponent;
  let fixture: ComponentFixture<LawyerCatalogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LawyerCatalogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LawyerCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
