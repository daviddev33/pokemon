import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LawyerCatalogService} from 'app/catalog/services/search-lawyer-service';
import {Lawyer} from 'app/catalog/types/interfaces/index';
import {LoadingService} from "../../../shared/services/loading.service";

@Component({
  selector: 'app-lawyer-catalog',
  templateUrl: './lawyer-catalog.component.html',
  styleUrls: ['./lawyer-catalog.component.scss']
})
export class LawyerCatalogComponent implements OnInit {

  public filteredLawyersList: Lawyer[] = [];
  public numberOfLawyers: number = 0;
  public searchParams: any;

  constructor(
    private lawyerCatalog: LawyerCatalogService,
    private route: ActivatedRoute,
    private loadingService: LoadingService
  ) {
  }

  async ngOnInit() {
    this.getSearchParams();
    await this.getDataLawyers(this.searchParams.service, this.searchParams.city);
  }

  getSearchParams() {
    this.route.queryParamMap.subscribe((params) => {
      const city = params.get('city');
      const service = params.get('service');
      this.searchParams = {city, service};
    });
  }

  //get the list of lawyers, execute the filter function and generate a new filtered list
  async getDataLawyers(specialty: string, city: string) {
    this.loadingService.setLoading(true);
   try {
       const dataResponse = await this.lawyerCatalog.getLawyersCatalog();
      const lawyersList = this.filterDataLawyers(dataResponse.lawyers, specialty, city);
      lawyersList.forEach(lawyer => this.filteredLawyersList.push(lawyer));
      this.numberOfLawyers = this.filteredLawyersList.length;
    } catch (err) {
      console.error(err)
    }
    ;
    this.loadingService.setLoading(false);
  }

  //function to filter list of lawyers according to user input city and specialty
  filterDataLawyers(list: Lawyer[], specialty: string, city: string) {
    if (city === 'todas las ciudades') {
      return list;
    }
    return list.filter(lawyer => {
      return lawyer.specialty.some(elem => elem.name.toLowerCase() === specialty) && lawyer.city.toLowerCase() === city
    });
  }

}
