import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Method, PayMethod} from "../../types/interfaces";

@Component({
  selector: 'app-lawyer-filter',
  templateUrl: './lawyer-filter.component.html',
  styleUrls: ['./lawyer-filter.component.scss']
})
export class LawyerFilterComponent implements OnInit {
  public payMethods: PayMethod[] = [];
  public form: FormGroup;
  public score: number[] = [1,2,3,4,5];

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.getPayMethods();
  }

  /**
   * Function to get payment methods
   */
  getPayMethods() {
    this.payMethods = [
      {
        label: 'Efectivo',
        value: Method.CASH
      },
      {
        label: 'Crédito',
        value: Method.CREDIT
      },
      {
        label: 'Paypal',
        value: Method.PAYPAL
      }
    ]
  }

  /**
   * Function to build form
   */
  buildForm() {
    this.form = this.formBuilder.group({
      virtualConsulting: [''],
      payMethod: [''],
      score: [''],
      from: [''],
      to: ['']
    })
  }

  /**
   * check if the given field is valid
   * @param field to be validated
   */
  public invalidField(field: string){
    const validator = this.form.get(field);
    return validator.touched && validator.invalid;
  }
}
