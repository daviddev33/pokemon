interface IconSVG {
  name: string;
  route: string;
}

export {
  IconSVG
};
