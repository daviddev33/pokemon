import {HomeRoutingModule} from './home/home-routing.module';
import {NgModule} from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {GeneralClassesComponent} from "./utils/general-classes/general-classes.component";

const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 25],
};
const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/inicio/cliente'},
  {path: 'classes', component: GeneralClassesComponent},
  {path: '**', redirectTo: '/inicio/cliente', pathMatch: 'full'},
];

@NgModule({
  declarations: [],
  imports: [
    HomeRoutingModule,
    RouterModule.forRoot(routes, routerOptions)
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {
}
