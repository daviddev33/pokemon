import {Component, NgZone, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CdkScrollable, ScrollDispatcher} from '@angular/cdk/scrolling';
import {fadeInOnEnterAnimation, fadeOutOnLeaveAnimation} from 'angular-animations';
import {map} from 'rxjs/operators';
//components
import {MessageCatalogueComponent} from '../../home/common-components/message-catalogue/message-catalogue.component';

@Component({
  selector: 'app-profile-lawyer',
  templateUrl: './profile-lawyer.component.html',
  styleUrls: ['./profile-lawyer.component.scss'],
  animations: [
    fadeInOnEnterAnimation({duration: 2000, delay: 100}),
  ]
})
export class ProfileLawyerComponent implements OnInit {
  /**
   * this attribute is define to active the animation when scroll is the position 12.
   * @private
   */
  private readonly SHRINK_TOP_SCROLL_POSITION = 12;
  public shrinkScroll = false;

  constructor(public dialog: MatDialog, private scrollDispatcher: ScrollDispatcher, private ngZone: NgZone) {
  }

  ngOnInit(): void {
    this.actionScroll();
  }

  /**
   * The functionality of this function is detect position of the scroll. Realize the expansion
   * or reduce the size the profile lawyer.
   * @private
   */
  private actionScroll() {
    this.scrollDispatcher.scrolled()
      .pipe(map((event: CdkScrollable) => this.getScrollPosition(event)))
      .subscribe(scrollTop => this.ngZone.run(() => this.shrinkScroll = scrollTop > this.SHRINK_TOP_SCROLL_POSITION)
      );
  }

  /**
   * catch the actual position scroll
   * @param event
   */
  getScrollPosition(event) {
    if (event) {
      return event.getElementRef().nativeElement.scrollTop;
    } else {
      return window.scrollY;
    }
  }

  /**
   * Open the dialog the component message
   */
  public message() {
    const section = 'message';
    this.dialog.open(MessageCatalogueComponent, {data: {section}, panelClass: 'container-message'});
  }

  /**
   * Open the dialog the component calendar
   */
  public calendar() {
    const section = 'calendar';
    this.dialog.open(MessageCatalogueComponent, {data: {section}, panelClass: 'container-message'});
  }

}
