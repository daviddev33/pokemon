import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Actions} from '../types/interfaces';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.scss']
})
export class LikeComponent implements OnInit {

  @Input() actions: Actions;
  @Output() likes: EventEmitter<number> = new EventEmitter<number>();
  @Output() dislikes: EventEmitter<number> = new EventEmitter<number>();
  public clickedLike = false;
  public clickedDislike = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  public dislikeClick() {
    this.likes.emit(this.actions.dislikes++);
    this.clickedDislike = !this.clickedDislike;
    if (this.clickedLike) {
      this.clickedLike = !this.clickedLike;
    }
  }

  public likeClick() {
    this.likes.emit(this.actions.likes++);
    this.clickedLike = !this.clickedLike;
    if (this.clickedDislike) {
      this.clickedDislike = !this.clickedDislike;
    }
  }
}
