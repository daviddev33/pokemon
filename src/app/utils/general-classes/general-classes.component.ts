import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertsService} from '@services/alerts/alerts.service';
import {LoadingService} from '../../shared/services/loading.service';
import {Lawyer, LawyerCardOptions} from '../../catalog/types/interfaces';

@Component({
  selector: 'app-general-classes',
  templateUrl: './general-classes.component.html',
  styleUrls: ['./general-classes.component.scss']
})
export class GeneralClassesComponent implements OnInit {
  public form: FormGroup;
  public form2: FormGroup;
  public isLoading = false;
  public lawyer: Lawyer = {
    name: 'Johanna Alejandra Pérez',
    office: 'Oficina 390',
    experience: 10,
    shortDescription: 'Abogado litigante que ayuda el lesionado a superar la injusticia. Disfruto ayudando.',
    description: 'Abogado litigante que ayuda el lesionado a superar la injusticia. Disfruto ayudando a mis clientes a través de los períodos dificiles de su vida.',
    score: 4,
    image: '../../../assets/svg/catalogue-lawyers/photo4.svg',
    phone: '(57 - 1)411 76 23',
    cellphone: '(57 - 1)314 419 53 21',
    webPage: 'https://www.jesusyepesabogados.com/',
    email: 'jperez@smartsoftlabs.com'
  };
  public lawyer2: Lawyer = {
    name: 'Johanna Pérez',
    experience: 10,
    shortDescription: 'Asesoría gratuita 30 minutos.',
    description: 'Asesoría gratuita 30 minutos.',
    score: 4,
    image: '../../../assets/svg/catalogue-lawyers/photo4.svg',
    phone: '(57 - 1)411 76 23',
    cellphone: '314 419 5321',
    webPage: 'https://www.jesusyepesabogados.com/',
    email: 'jperez@smartsoftlabs.com'
  };
  public options1: LawyerCardOptions = {
    background: "white-bg",
    contactOptions: true,
    description: false,
    premiumMark: false,
  };
  public options3: LawyerCardOptions = {
    background: "blue-bg",
    contactOptions: false,
    description: true,
    premiumMark: false,
    office: false
  };
  public options2: LawyerCardOptions = {
    background: "white-bg",
    contactOptions: true,
    description: true,
    premiumMark: false,
  };
  public options4: LawyerCardOptions = {
    background: "white-bg",
    contactOptions: false,
    description: false,
    premiumMark: false,
  };
  constructor(
    private formBuilder: FormBuilder,
    private alert: AlertsService,
    private loadingService: LoadingService
  ) {
  }

  public async ngOnInit() {
    await this.initForm();
    this.loadingService.setLoading(false);
  }

  public async initForm() {
    this.form = this.formBuilder.group({
      first: ['', [Validators.required, Validators.minLength(3)]],
      second: ['', [Validators.required, Validators.minLength(3)]],
      selector: ['', [Validators.required]],
      textArea: ['', [Validators.required]]
    });
    this.form2 = this.formBuilder.group({
      test: ['', [Validators.required, Validators.minLength(3)]],
      secondT: ['', [Validators.required, Validators.minLength(3)]],
      selectorT: ['', [Validators.required]],
      textAreaT: ['', [Validators.required]]
    });
  }

  /**
   * check if the given field is valid
   * @param field to be validated
   */
  public invalidField(field: string) {
    const validator = this.form.get(field);
    return validator?.touched && validator?.invalid;
  }

  /**
   * check if the given field is valid
   * @param field to be validated
   */
  public invalidField2(field: string) {
    const validator = this.form2.get(field);
    return validator.touched && validator.invalid;
  }

  public turnOnLoading() {
    this.form2.markAllAsTouched();
    this.isLoading = true;
    setTimeout(() => this.isLoading = false, 1000);
  }

  public async turnOnLoading2() {
    this.form.markAllAsTouched();
    this.loadingService.setLoading(false);
    setTimeout(() => this.loadingService.setLoading(false), 1000);

  }

  public warningToaster() {
    this.alert.showWarning('Ejemplo de warning con un texto relativamente largo');
  }

  public infoToaster() {
    this.alert.showInformation('Ejemplo de info con un texto relativamente largo con tiempo para leer');
  }

  public successToaster() {
    this.alert.showSuccess('Ejemplo de success con un texto relativamente largo');
  }

  public errorToaster() {
    this.alert.showError('Ejemplo de error con un texto relativamente largo, sin cerrarse hasta que el usuario de click en el toaster');
  }
}
