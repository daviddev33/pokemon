import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import {Lawyer, LawyerCardOptions} from '../../catalog/types/interfaces';
import {MatDialog} from '@angular/material/dialog';
import {MessageCatalogueComponent} from '../../home/common-components/message-catalogue/message-catalogue.component';
import {fadeInOnEnterAnimation, fadeOutOnLeaveAnimation} from "angular-animations";

@Component({
  selector: 'app-lawyer-card',
  templateUrl: './lawyer-card.component.html',
  styleUrls: ['./lawyer-card.component.scss'],
  animations: [
    fadeInOnEnterAnimation({delay:500}),
    fadeOutOnLeaveAnimation({duration:100})
  ]
})
export class LawyerCardComponent implements OnInit{
  @Input() lawyer: Lawyer;
  @Input() options: LawyerCardOptions = {
    avatar: true,
    background: 'white-bg',
    contactOptions: true,
    description: true,
    hideInformation: false,
    office: true,
    premiumMark: false
  };

  constructor(
    public dialog: MatDialog){
  }

  ngOnInit(): void {
  }

  /**
   * Open the dialog
   * @param section to be opened on the mat dialog component.
   */
  public openDialog(section: 'calendar' | 'message') {
    this.dialog.open(MessageCatalogueComponent, {data: {section}, panelClass: 'container-message'});
  }
}
