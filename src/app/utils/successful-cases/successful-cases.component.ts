import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../services/utils.service';
import {Case} from '../types/interfaces';
import {LoadingService} from '../../shared/services/loading.service';

@Component({
  selector: 'app-successful-cases',
  templateUrl: './successful-cases.component.html',
  styleUrls: ['./successful-cases.component.scss']
})
export class SuccessfulCasesComponent implements OnInit {
  public panelOpenState: boolean = false;
  public cases: Array<Case & { state: boolean }>;

  constructor(private utilService: UtilsService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    await this.getData();
  }

  private async getData() {
    this.loadingService.setLoading(true);
    try {

      const data = await this.utilService.getDescriptionLawyer();
      /**
       * In this case was accomplished a map to take the array of cases in the file json and
       * can add a new variable.
       */
      const dataCase = data.map((value) => {
        return value.cases;
      });
      this.cases = [];
      for (const item of dataCase[0]) {
        this.cases.push({...item, state: false});
      }
    } catch (e) {
      console.error(e);
    }
    this.loadingService.setLoading(false);
  }
}
