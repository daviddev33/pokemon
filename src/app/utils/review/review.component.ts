import { Component, OnInit } from '@angular/core';
import {UtilsService} from '../services/utils.service';
import {ReviewLawyer} from '../types/interfaces';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  /**
   * number pages and page initial of the paginator.
   */
  public pageSize : number = 2;
  public pageNumber = 1;
  public sizeScreen : number;
  public reviewLawyer: Array<ReviewLawyer & {state:boolean}>;
  /**
   * attribute to starts
   */
  public score: number = 4;
  constructor( private utilsServices: UtilsService) { }
 async ngOnInit() {
  await this.getData();
  }

  public async getData(){
    const data = await this.utilsServices.getReviewLawyer();
    this.reviewLawyer = [];
    for (const [index, item] of data.entries()) {
    this.reviewLawyer.push({...item, state:false});
    }
  }

  public handlePage(event: number) {
    this.pageNumber = 0;
    this.pageNumber = event;
  }
}
