export interface DescriptionLawyer {
  description: string;
  areas: Area[];
  cases: Case[];
}
export interface Area {
  icon: string;
  name: string;
}
export interface Case {
  name: string;
  subTitle: string;
  year: string;
  description: string;
  plusDescription: string;
}
