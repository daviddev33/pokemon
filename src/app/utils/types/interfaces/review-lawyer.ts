export interface ReviewLawyer {
  title: string;
  date: string;
  description: string;
}
