import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
// Components Utils
import { CardComponent } from './card/card.component';
import { DialogComponent } from './dialog/dialog.component';
import { MaterialDesignerModule } from 'app/material-designer/material-designer.module';
import { MatButtonModule } from '@angular/material/button';

// Utils
import { PaginatePipe } from './pipes/paginate.pipe';
import { LikeComponent } from './like/like.component';
import { PaymentsComponent } from './payments/payments.component';
import { EducationComponent } from './education/education.component';
import { ReviewComponent } from './review/review.component';
import { ShortNumberPipe } from './pipes/short-number.pipe';
import {LawyerCardComponent} from './lawyer-card/lawyer-card.component';
import { SocialMediaComponent } from './social-media/social-media.component';


import { PaginatorComponent } from './paginator/paginator.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ExperienceComponent } from './experience/experience.component';
import { SimilarLawyersComponent } from './similar-lawyers/similar-lawyers.component';
import { ProfileLawyerComponent } from './profile-lawyer/profile-lawyer.component';
import { CustomSwiperComponent } from './custom-swiper/custom-swiper.component';
import {SwiperModule} from 'swiper/angular';
import {GeneralClassesComponent} from './general-classes/general-classes.component';
import { DescriptionLawyerComponent } from './description-lawyer/description-lawyer.component';
import { SuccessfulCasesComponent } from './successful-cases/successful-cases.component';
import { PlanComponent } from './plan/plan.component';
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [
    CardComponent,
    DialogComponent,
    PaginatePipe,
    LikeComponent,
    PaymentsComponent,
    EducationComponent,
    ReviewComponent,
    ShortNumberPipe,
    LawyerCardComponent,
    SocialMediaComponent,
    PaginatorComponent,
    ExperienceComponent,
    SimilarLawyersComponent,
    ProfileLawyerComponent,
    CustomSwiperComponent,
    ProfileLawyerComponent,
    GeneralClassesComponent,
    DescriptionLawyerComponent,
    SuccessfulCasesComponent,
    PlanComponent,
  ],
    imports: [
        CommonModule,
        MaterialDesignerModule,
        MatButtonModule,
        ReactiveFormsModule,
        FormsModule,
        InlineSVGModule,
        SwiperModule,
        SharedModule,
    ],
  exports: [
    DialogComponent,
    CardComponent,
    PaginatePipe,
    LikeComponent,
    PaymentsComponent,
    EducationComponent,
    LawyerCardComponent,
    SocialMediaComponent,
    PaginatorComponent,
    ExperienceComponent,
    SimilarLawyersComponent,
    ProfileLawyerComponent,
    CustomSwiperComponent,
    DescriptionLawyerComponent,
    SuccessfulCasesComponent,
    PlanComponent,
    ReviewComponent,
  ]
})
export class UtilsModule { }
