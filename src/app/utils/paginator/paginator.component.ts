import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
  @Input() totalItemsCount: number;
  @Input() itemsPerPage: number;
  @Input() initialPage: number = 0;
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
  @Input() useRowDirection: boolean;
  public inactiveButtonPrev = true;
  public inactiveButtonNext = false;
  public sizeScreen: number;
  public flag:number = 0;

  constructor(){
  }

  ngOnInit(): void {
    this.sizeScreen = window.innerWidth;
    window.addEventListener('resize', () => {
      this.sizeScreen = window.innerWidth;
    })
  }

  public nextPage(status: string) {
    switch (status) {
      case'prev': {
        this.initialPage--;
        if (this.initialPage > 0) {
          this.checkInactiveButtonPrev();
          this.checkInactiveButtonNext();
        } else {
          this.initialPage = 1;
        }
        this.pageChange.emit(this.initialPage);
        break;
      }
      case 'next': {
        if ((this.initialPage * this.itemsPerPage) >= this.totalItemsCount) {
          this.checkInactiveButtonNext();
        } else {
          this.initialPage++;
          this.checkInactiveButtonPrev();
          this.checkInactiveButtonNext();
        }
        this.pageChange.emit(this.initialPage);
        break;
      }
    }
  }

  /**
   * show the total pages in the paginator
   */
  public totalPages(){
    return Math.ceil(this.totalItemsCount /  this.itemsPerPage);
  }

  /**
   * use the class to deactivate when you stay on the last page - next button.
   */
  public checkInactiveButtonPrev() {
      this.inactiveButtonPrev =  this.initialPage == 1;
  }

  /**
   * use the class to deactivate when stay in the first paginate - next button.
   */
  public checkInactiveButtonNext() {
    this.inactiveButtonNext = (this.initialPage * this.itemsPerPage) >= this.totalItemsCount;
  }

  public get changePaginator() {
    return this.useRowDirection && this.sizeScreen >= 1024;
  }

}



