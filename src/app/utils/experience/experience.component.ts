import {ExperienceElement} from '../types/interfaces';
import {UtilsService} from '../services/utils.service';
import {Component, OnInit} from '@angular/core';
import {LoadingService} from "../../shared/services/loading.service";

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  /**
   * add type of data to the attribute experience and add the new attribute
   * for the button expansion panel.
   */
  public experience: Array<ExperienceElement & { open: boolean }>;
  public panelOpenState = false;
  public test: string;

  constructor(private utilsService: UtilsService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    await this.getData();
  }

  /**
   * Take the data of the file json with static data (for the moment).
   * @private
   */
  private async getData() {
    this.loadingService.setLoading(true);
    try {

      const data = await this.utilsService.getExperience();
      this.experience = [];
      for (const item of data.experience) {
        this.experience.push(
          {...item, open: false}
        );
      }
    } catch (error) {
      console.error(error);

    }
    this.loadingService.setLoading(false);
  }
}
