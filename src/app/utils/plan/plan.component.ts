import {Component, Input, OnInit} from '@angular/core';
import {Plan} from '../../home/lawyer/types/interfaces';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {
@Input() plan : Plan;
@Input() isChecked : boolean;
@Input() functionality : string[];
  constructor() { }
  ngOnInit(): void {
  }
}
