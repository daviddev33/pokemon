import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../services/utils.service';
import {Area, DescriptionLawyer} from '../types/interfaces';
import {LoadingService} from '../../shared/services/loading.service';

@Component({
  selector: 'app-description-lawyer',
  templateUrl: './description-lawyer.component.html',
  styleUrls: ['./description-lawyer.component.scss']
})
export class DescriptionLawyerComponent implements OnInit {
  public descriptionLawyer: Array<DescriptionLawyer>;
  public area;
  public panelOpenState;
  panelOpenStateAreas = false;

  constructor(private utilsService: UtilsService,
              private loadingService: LoadingService) {
  }

  public union: string;

  async ngOnInit() {
    await this.getData();
  }

  /**
   * In this method consume the service to tak the data of the file json.
   */
  public async getData() {
    this.loadingService.setLoading(true);
    try {
      this.descriptionLawyer = await this.utilsService.getDescriptionLawyer();
      this.getTest(this.descriptionLawyer[0].description);
      this.area = this.descriptionLawyer.map((data) => {
        return data.areas;
      });
      this.area = this.area[0];
    } catch (e) {
      console.error(e);
    }
    this.loadingService.setLoading(false);
  }

  /**
   * it is getting the description of the lawyer to split the string in two
   * In this moment don't work.
   * @param data
   */
  public getTest(data: string) {
    this.union = ' ';
    let value = data.split(' ').length;
    value = Math.round(value / 2);
    let a = data.split(' ', value);
    let b = a.toString();
    this.union = b.split(',').join(' ');
  }
}
