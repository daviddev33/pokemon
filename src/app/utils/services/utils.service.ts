import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Interfaces
import {Education, Payments, Experience, ReviewLawyer} from '../types/interfaces';
import {DescriptionLawyer} from '../types/interfaces';
import {Lawyer} from "../../catalog/types/interfaces";

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor(private http: HttpClient) {
  }

  public getPayments() {
    return this.http.get<Payments>('../../assets/data/payments.json').toPromise();
  }

  public getEducation() {
    return this.http.get<Education>('../../assets/data/education.json').toPromise();
  }
  public getExperience() {
    return this.http.get<Experience>('../../assets/data/experience.json').toPromise();
}
  public getSimilarLawyer() {
    return this.http.get<any>('../../assets/data/similar-lawyer.json').toPromise();
  }
  public getDescriptionLawyer() {
    return this.http.get<DescriptionLawyer[]>('../../assets/data/description-lawyer.json').toPromise();
}
  public getReviewLawyer() {
    return this.http.get<ReviewLawyer[]>('../../assets/data/review-lawyer.json').toPromise();
  }
}
