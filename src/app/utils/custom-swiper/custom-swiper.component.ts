import {Component, HostBinding, Input} from '@angular/core';
import {SwiperComponent} from "swiper/angular";
import {PaginationOptions} from 'swiper/types/components/pagination';
import {type} from 'os';

@Component({
  selector: 'app-custom-swiper',
  templateUrl: './custom-swiper.component.html',
  styleUrls: ['./custom-swiper.component.scss']
})
export class CustomSwiperComponent extends SwiperComponent {

  @Input()
  style = 'background-color: white';

  @Input()
  set pagination(val: boolean | CustomPaginationOptions) {
    let newVal: CustomPaginationOptions;
    if(!val || typeof val === 'boolean'){
      newVal = {
        clickable: true,
        dynamicBullets: true,
        position: 'BOTTOM'
      }
    } else {
      newVal = val;
    }
    super.pagination = newVal;
    this._customPagination = super.pagination;
    this._customPagination.position = this._customPagination.position || 'BOTTOM';
  }
  get pagination() {
    return <CustomPaginationOptions>super.pagination;
  }

  private _customPagination: CustomPaginationOptions;

  get customPagination(){
    return this._customPagination;
  }

  // don't extend constructor
  ngOnInit() {
    super.ngOnInit();
  }

  getTranslation(){
    let offset = -50;
    if(this.swiperRef){
      const count = (<{ bullets: Array<any> }>this.swiperRef.pagination).bullets.length;
      const index = this.swiperRef.activeIndex;
      // left side
      if(index === 0){
        if(count >= 3){
          offset = offset - 18;
        } else if(count === 2){
          offset = offset - 8;
        }
      } else if(index === 1 && index !== count -1){
        if(count > 3){
          offset = offset - 10;
        }
      }
      // right side
      else if(index === count -1){
        if(count >= 3){
          offset = offset + 18;
        } else if(count === 2){
          offset = offset + 8;
        }
      } else if(index === count -2){
        if(count >= 3){
          offset = offset + 10;
        }
      }
    }
    return `transform: translate(${offset}% ) !important;`;
  }
}

export interface CustomPaginationOptions extends PaginationOptions{
  position?: "TOP" | "BOTTOM";
}
