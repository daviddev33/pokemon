import {Component, OnInit} from '@angular/core';

// Interfaces
import {Rate, OtherRate, MethodPay} from '../types/interfaces/payments';

// Services
import {UtilsService} from '../services/utils.service';
import {LoadingService} from "../../shared/services/loading.service";

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  constructor(private utilsService: UtilsService,
              private loadingService: LoadingService) {
  }

  public rate: Rate[];
  public otherRate: OtherRate[];
  public methodPay: MethodPay[];

  async ngOnInit() {
    this.loadingService.setLoading(true);
    try {

      const payments = await this.utilsService.getPayments();
      this.rate = payments.rate;
      this.otherRate = payments.other_rate;
      this.methodPay = payments.method_pay;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }
}
