import {EducationElement} from '../types/interfaces';
import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../services/utils.service';
import {LoadingService} from "../../shared/services/loading.service";

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {
  constructor(private utilsService: UtilsService,
              private loadingService: LoadingService) {
  }

  panelOpenState = false;
  public information: EducationElement[];

  async ngOnInit() {
    await this.getData();
  }

  /**
   * Consume the service where take the data of the file json.
   * @private
   */
  private async getData() {
    this.loadingService.setLoading(true);
   try {
       const education = await this.utilsService.getEducation();
      this.information = education.education;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }
}
