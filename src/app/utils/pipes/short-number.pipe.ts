import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortNumber'
})
export class ShortNumberPipe implements PipeTransform {

  transform(number: number): string {
    //validate that if it is a number
    if(isNaN(number)) return null;
    if (number === null) return null;
    if(number === 0) return null;
    let absolute = Math.abs(number);
    let key = '';
    const rounder = Math.pow(10, 1)

    const letters = [
      { key: 'M', value: Math.pow(10, 6)},
      { key: 'K', value: Math.pow(10, 3)}      
    ];

    for(let obj of letters) {
      let numberShort = absolute / obj.value;
      numberShort = Math.round(numberShort * rounder) / rounder;
      if(numberShort >= 1) {
        absolute = numberShort;
        key = obj.key;
        break;
      }
    }
    return `${absolute}${key}`
  }
}
