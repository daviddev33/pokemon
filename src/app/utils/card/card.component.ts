import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() lastNews = [];
  @Input() pageSize: number;
  @Input() pageNumber: number;
  @Output() eventCard: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  async ngOnInit() {
  }

  public async onClick(event: any) {
    this.eventCard.emit(event);
  }
}
