import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Footer } from '../../home/client/types/interfaces';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  constructor(private http: HttpClient) { }
  public nameLogo() {
    return this.http.get<Footer>('../../assets/data/footer.json').toPromise();
  }
}
