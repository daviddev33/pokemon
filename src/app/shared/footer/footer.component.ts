import {Icon} from '../../home/client/types/interfaces';
import {Component, OnInit} from '@angular/core';
import {SharedService} from '../services/shared.service';
import {logWarnings} from 'protractor/built/driverProviders';
import {LoadingService} from "../services/loading.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public icon: Icon[];
  public textLegal: string;

  constructor(private serviceShared: SharedService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    this.loadingService.setLoading(true);
    try {

      const data = await this.serviceShared.nameLogo();
      this.textLegal = data.title;
      this.icon = data.icon;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  public changeColor(data) {
    console.log(data);
  }

  public test(svg: SVGElement, parent: Element | null): SVGElement {
    console.log(svg);

    svg.children[0].attributes.getNamedItem("fill").value = "red"
    return svg;
  }
}
