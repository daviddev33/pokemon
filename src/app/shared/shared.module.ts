import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InlineSVGModule} from 'ng-inline-svg';

// Modules
import {MaterialDesignerModule} from '../material-designer/material-designer.module';

// Components
import {FooterComponent} from './footer/footer.component';
import {RouterModule} from '@angular/router';
import { LoadingDirective } from './directives/loading.directive';

@NgModule({
  declarations: [
    FooterComponent,
    LoadingDirective,
  ],
    imports: [
        CommonModule,
        MaterialDesignerModule,
        RouterModule,
        InlineSVGModule
    ],
  exports: [
    FooterComponent,
    LoadingDirective
  ],
})
export class SharedModule {
}
