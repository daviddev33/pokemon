import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';

// Components
import {LawyerComponent} from './lawyer.component';
import {ComparisonPlansComponent} from './components/comparison-plans/comparison-plans.component';

const routes: Routes = [
  {
    path: '',
    component: LawyerComponent
  },
  {
    path: 'comparacion',
    component: ComparisonPlansComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RouterModule
  ],
  exports: [
    RouterModule
  ]
})
export class LawyerRoutingModule {
}
