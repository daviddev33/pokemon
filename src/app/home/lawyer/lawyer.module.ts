import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { SwiperModule } from "swiper/angular";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Modules
import { LawyerRoutingModule } from './lawyer-routing.module';
import { MaterialDesignerModule } from '../../material-designer/material-designer.module';
import { UtilsModule } from 'app/utils/utils.module';
// Components
import { LawyerComponent } from './lawyer.component';
import { HeroLawyerComponent } from './components/hero-lawyer/hero-lawyer.component';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { SharedModule } from 'app/shared/shared.module';
import { ToolsLawyerComponent } from './components/tools-lawyer/tools-lawyer.component';
import { PlansComponent } from './components/plans/plans.component';
import { ComparisonPlansComponent } from './components/comparison-plans/comparison-plans.component';

@NgModule({
  declarations: [
    LawyerComponent,
    HeroLawyerComponent,
    ToolsLawyerComponent,
    PlansComponent,
    ComparisonPlansComponent
  ],
  imports: [
    CommonModule,
    LawyerRoutingModule,
    CommonComponentsModule,
    MaterialDesignerModule,
    UtilsModule,
    SharedModule,
    InlineSVGModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    LawyerComponent,
    HeroLawyerComponent
  ]
})
export class LawyerModule { }
