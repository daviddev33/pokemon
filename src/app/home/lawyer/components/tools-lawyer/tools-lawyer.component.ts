import {Component, OnInit} from '@angular/core';
import {Tool, ToolsLawyer} from '../../types/interfaces';
import {LawyerService} from '../services/lawyer.service';
import {LoadingService} from "../../../../shared/services/loading.service";

@Component({
  selector: 'app-tools-lawyer',
  templateUrl: './tools-lawyer.component.html',
  styleUrls: ['./tools-lawyer.component.scss']
})
export class ToolsLawyerComponent implements OnInit {
  public tools: Array<Tool & { open: boolean }>;

  constructor(private lawyerService: LawyerService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    await this.getData();
  }

  /**
   * consume the service where take the data in the file json
   * @private
   */
  private async getData() {
    this.loadingService.setLoading(true);
    try {

      const data = await this.lawyerService.getToolsLawyer();
      this.tools = [];
      for (const item of data.tools) {
        this.tools.push({...item, open: false}
        );
      }
    } catch (e) {
      console.error(e);
    }
    this.loadingService.setLoading(false);
  }
}
