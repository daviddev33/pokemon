import {Component, OnInit} from '@angular/core';
import {LawyerService} from '../services/lawyer.service';
import {Plan} from '../../types/interfaces';
import {LoadingService} from "../../../../shared/services/loading.service";

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
  public plans: Array<Plan & { open: boolean }>;
  public isChecked: boolean = false;

  constructor(private lawyerService: LawyerService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    await this.getData()
  }

  /**
   * this function has purpose of consume the service and add a new attribute boolean and identify what
   * plan need the gold ribbon. Also, it adds another attribute boolean for the animation plus and minus.
   * @private
   */
  private async getData() {
    this.loadingService.setLoading(true);
    try {

      const data = await this.lawyerService.getPlans();
      this.plans = [];
      for (const index in data) {
        /**
         * in this case have added a boolean for expand or reduce the mat expand
         */
        this.plans.push({...data[index], open: false})
      }
    } catch (e) {
      console.error(e);
    }
    this.loadingService.setLoading(false);
  }
}
