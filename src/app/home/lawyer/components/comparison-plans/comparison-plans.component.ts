import {Component, OnInit} from '@angular/core';
import SwiperCore, {Pagination, Navigation} from "swiper/core";
import {LawyerService} from '../services/lawyer.service';
import {functionality, Plan} from '../../types/interfaces';
import {LoadingService} from '../../../../shared/services/loading.service';

SwiperCore.use([Pagination, Navigation]);

@Component({
  selector: 'app-comparison-plans',
  templateUrl: './comparison-plans.component.html',
  styleUrls: ['./comparison-plans.component.scss']
})
export class ComparisonPlansComponent implements OnInit {
  /**
   * State slide
   * @public isChecked
   */
  public isChecked: boolean = false;
  public plans: Array<Plan>;
  public functionality: string[];

  constructor(private lawyerService: LawyerService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    await this.getData();
  }

  /**
   * Consume the service to take the data of the json file. Also, it was added
   * a new variable to show the icons success and cross.
   * @private
   */
  private async getData() {
    this.loadingService.setLoading(true);
    try {

      this.plans = await this.lawyerService.getPlans();
      this.functionality = this.plans[0].functionalities.map((value) => {
        return value.name
      });
    } catch (e) {
      console.error(e);

    }
    this.loadingService.setLoading(false);
  }
}
