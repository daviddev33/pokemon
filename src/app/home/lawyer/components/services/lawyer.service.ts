import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToolsLawyer, Plan} from '../../types/interfaces';

@Injectable({
  providedIn: 'root'
})
export class LawyerService {

  constructor( private http: HttpClient) { }
  public getToolsLawyer(){
    return this.http.get<ToolsLawyer>('../../../../../assets/data/tools-lawyer.json').toPromise();
  }
  public getPlans(){
    return this.http.get<Plan[]>('../../../../../assets/data/plans.json').toPromise();
  }
}
