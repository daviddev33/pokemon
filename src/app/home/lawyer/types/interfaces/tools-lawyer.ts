export interface ToolsLawyer {
  tools: Tool[];
}

export interface Tool {
  icon: string;
  title: string;
  description: string;
}
