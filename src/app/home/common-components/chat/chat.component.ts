import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertsService} from '@services/alerts/alerts.service';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  public message: FormControl;
  public changeChat: string;
  public testMessage: string[] = [];
  public bot: boolean;
  /**
   * characters to validate email
   * @private
   */
  private regexEmail = '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$';
  public formMessage: FormGroup;
  public bubble: HTMLCollection;
  public textArea: HTMLAllCollection;

  constructor(private fb: FormBuilder, private alert: AlertsService) {
    this.message = new FormControl('');
    this.changeChat = 'bot';
    this.bot = true;
    /**
     * validations form message
     */
    this.formMessage = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.pattern(/^[a-zA-Z]*$/)]],
      email: ['', [Validators.required, Validators.pattern(this.regexEmail)]],
      phone: ['', [Validators.required, Validators.pattern(/[0-9+\- ]/)]],
      message: ['', [Validators.required, Validators.minLength(20)]]
    });
  }

  ngOnInit(): void {
    this.bubble = document.getElementsByClassName('content-chat');
  }

  public messageChat(event: any) {
    /**
     * quit the behavior default the area
     */
    event.preventDefault();
    event.stopPropagation();
    /**
     * Change the section when write the word "mensaje", clear the variables
     * and reset form.
     */
    if (this.message.value === 'mensaje') {
      this.changeChat = this.message.value;
      this.testMessage = [];
      this.message.reset();
    }
    /**
     * Change the section when write the word "asesor", clear the variables
     * and reset input.
     */
    else if (this.message.value === 'asesor') {
      this.changeChat = this.message.value;
      this.testMessage = [];
      this.message.reset();
    } else {
      /**
       *listen the scroll in the content chat and position the last bubble
       * always visual.
       */
      this.message.value?.length > 0 ? this.testMessage.push(this.message.value) : this.message.reset();
      setTimeout(() => {
        this.bubble[0].scrollTop = this.bubble[0].scrollHeight;
      }, 20);
      this.message.reset();
    }
  }

  /**
   * validation if the data is correct
   */
  public send() {
    if (this.formMessage.valid) {
      this.formMessage.reset();
    } else {
      this.formMessage.markAllAsTouched();
      this.alert.showWarning('Ingrese todos los Campos Requeridos', '');
    }
  }

  /**
   * receive the name of the input (formControlName) of form and realize the validation
   * @param data
   */
  public validation(data: string) {
    const validator = this.formMessage.get(data);
    return validator.touched && validator.invalid;
  }
}
