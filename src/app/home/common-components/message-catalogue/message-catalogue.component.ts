import {ScheduleService} from '../../client/services/schedule.service';
import {Component, OnInit, Inject} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import * as _rollupMoment from 'moment';
import * as _moment from 'moment';
import 'moment/locale/es';
import {LoadingService} from "../../../shared/services/loading.service";
import {Lawyer, LawyerCardOptions, DateElement} from '../../../catalog/types/interfaces';
const moment = _rollupMoment || _moment;
export const MY_FORMATS = {
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
  },
};

@Component({
  selector: 'app-message-catalogue',
  templateUrl: './message-catalogue.component.html',
  styleUrls: ['./message-catalogue.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'}
  ],
})
export class MessageCatalogueComponent implements OnInit {
  public dataDate: DateElement[];
  public NameLawyer: string;
  public date = new FormControl(moment());
  public pickDate: string;
  public pickHour: string;
  public lawyer: Lawyer;
  public options: LawyerCardOptions = {
    background: "white-bg",
    contactOptions: false,
    description: false,
    premiumMark: true,
    office: true
  };

  /**
   * send what dialog is open
   * @param option
   * Consume the service to lawyer
   * @param service
   * service to the loading.
   * @param loadingService
   */
  constructor(@Inject(MAT_DIALOG_DATA) public option: { section: string },
              public service: ScheduleService,
              private loadingService: LoadingService) {
  }
  selected: Date | null;

  async ngOnInit(){
    await this.getDates();
  }

  public async getDates() {
    this.loadingService.setLoading(true);
    try {
      const data = await this.service.getSchedule();
      this.dataDate = data.date;
      this.lawyer = data;
    } catch (error) {
    }
    this.loadingService.setLoading(false);
  }

  /**
   * take date and hour of the calendar when schedule a date.
   * @param date
   * @param hour
   */
  public scheduleDate(date: FormControl, hour: string) {
    this.option.section = 'confirmation';
    this.pickDate = moment(date.value).format('dddd LL').charAt(0).toUpperCase() +  moment(date.value).format('dddd LL').slice(1);
    this.pickHour = hour;
  }

  public confirmSchedule(option: string) {
    option === 'success' ? this.option.section = 'success' : this.option.section = 'calendar';
  }
}
