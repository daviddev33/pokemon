import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageCatalogueComponent } from './message-catalogue.component';

describe('MessageCatalogueComponent', () => {
  let component: MessageCatalogueComponent;
  let fixture: ComponentFixture<MessageCatalogueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessageCatalogueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageCatalogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
