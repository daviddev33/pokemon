import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InlineSVGModule} from 'ng-inline-svg';

// Modules
import { SwiperModule } from 'swiper/angular';
import { MaterialDesignerModule } from 'app/material-designer/material-designer.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UtilsModule } from 'app/utils/utils.module';
// Components
import { AdvantageComponent } from './advantage/advantage.component';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { IndicatorsComponent } from './indicators/indicators.component';
import { DownloadAppComponent } from './download-app/download-app.component';
import { LastNewsComponent } from './last-news/last-news.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { ChatComponent } from './chat/chat.component';
import { TestComponent } from './test/test.component';
import { MessageCatalogueComponent } from './message-catalogue/message-catalogue.component';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [
    AdvantageComponent,
    HowItWorkComponent,
    IndicatorsComponent,
    DownloadAppComponent,
    LastNewsComponent,
    NewsletterComponent,
    ChatComponent,
    TestComponent,
    MessageCatalogueComponent,
  ],
  imports: [
    CommonModule,
    SwiperModule,
    MaterialDesignerModule,
    ReactiveFormsModule,
    FormsModule,
    UtilsModule,
    InlineSVGModule,
    RouterModule
  ],
  exports: [
    AdvantageComponent,
    HowItWorkComponent,
    IndicatorsComponent,
    DownloadAppComponent,
    LastNewsComponent,
    NewsletterComponent,
    ChatComponent,
    TestComponent,
    MessageCatalogueComponent
  ]
})
export class CommonComponentsModule { }
