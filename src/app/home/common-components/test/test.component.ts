import { ChatComponent } from '../chat/chat.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  constructor(public dialog: MatDialog) { }
  ngOnInit(): void {}
  public chat(){
    this.dialog.open(ChatComponent, {width: '80%', panelClass: 'container-chat', backdropClass: 'styles-overlay-chat'});
}
}
