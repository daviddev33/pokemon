import {Component, Input, OnInit} from '@angular/core';

// Services
import {DataService} from '../../client/services/data.service';

// Interfaces
import {Indicators} from '../../client/types/interfaces'
import {LoadingService} from "../../../shared/services/loading.service";

@Component({
  selector: 'app-indicators',
  templateUrl: './indicators.component.html',
  styleUrls: ['./indicators.component.scss']
})
export class IndicatorsComponent implements OnInit {
  @Input() public indicators: Indicators[];
  @Input() public role: string;

  constructor(private dataService: DataService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    this.loadingService.setLoading(true);
   try {
       const response = await this.dataService.getIndicators(this.role);
      this.indicators = response.indicators;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }
}
