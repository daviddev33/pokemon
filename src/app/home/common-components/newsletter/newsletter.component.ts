import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';

// Services
import {AlertsService} from '@services/alerts/alerts.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {
  public formNews: FormGroup;
  private regexEmail = '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$';

  constructor(private fb: FormBuilder,
              private toast: AlertsService,
              private loadingService: LoadingService) {
    this.formNews = this.fb.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern(this.regexEmail)]],
    });
  }

  ngOnInit(): void {
  }

  public notification() {
    this.loadingService.setLoading(true);
    try {

      if (!this.formNews.valid) {
        this.toast.showError('Ingrese un email válido', '');
      } else {
        this.toast.showSuccess('Email registrado', '');
        this.formNews.reset();
      }
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  get emailField() {
    const validator = this.formNews.get('email');
    return validator.touched && validator.invalid;
  }
}
