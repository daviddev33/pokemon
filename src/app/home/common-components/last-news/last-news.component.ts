import {Component, OnInit} from '@angular/core';
import SwiperCore, {Pagination, Autoplay} from 'swiper/core';
import {ActivatedRoute, Router} from '@angular/router';

// Services
import {AlertsService} from '@services/alerts/alerts.service';
import {DataService} from '../../client/services/data.service';
import {LastNews} from "../../client/types/interfaces";
import {LoadingService} from "../../../shared/services/loading.service";

SwiperCore.use([Pagination, Autoplay]);

@Component({
  selector: 'app-last-news',
  templateUrl: './last-news.component.html',
  styleUrls: ['./last-news.component.scss']
})
export class LastNewsComponent implements OnInit {
  public lastNews;
  public blogs: LastNews[] = [];

  constructor(private dataService: DataService,
              private toast: AlertsService,
              private router: Router,
              private activatedRouter: ActivatedRoute,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    await this.getData();
  }

  /**
   * In this functions consume the service DataService, bring the blog list
   */
  private async getData() {
    this.loadingService.setLoading(true);
   try {const response = await this.dataService.lastNews();
      this.lastNews = response.news;
      for (const lastNew of this.lastNews) {
        if (this.blogs.length < 5) {
          this.blogs.push(lastNew);
        }
      }
    } catch (error) {
      this.toast.showError(error, 'Error!');
    }
    this.loadingService.setLoading(false);
  }

  public async onClick(id) {
    await this.router.navigate(['./blogs', id], {relativeTo: this.activatedRouter});
  }
}
