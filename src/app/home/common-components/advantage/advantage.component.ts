import {Component, Input, OnInit} from '@angular/core';

// Service
import {ClientService} from '../../client/services/client.service';

// Interfaces
import {AdvantageElement} from '../../client/types/interfaces';
import {LoadingService} from "../../../shared/services/loading.service";

@Component({
  selector: 'app-advantage',
  templateUrl: './advantage.component.html',
  styleUrls: ['./advantage.component.scss']
})
export class AdvantageComponent implements OnInit {
  @Input() public dataAdvantage: AdvantageElement[];
  @Input() public role: string;
  @Input() public mainDataAdvantage: AdvantageElement[];

  constructor(private clientService: ClientService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    this.loadingService.setLoading(true);
    try {
      const data = await this.clientService.getAdvantage(this.role);
      const mainData = await this.clientService.getMainAdvantage(this.role);
      this.dataAdvantage = data.advantage;
      this.mainDataAdvantage = mainData.advantage;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }
}
