import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

// Services
import {ClientService} from './services/client.service';
import {DataService} from './services/data.service';
// Interfaces
import {AdvantageElement} from './types/interfaces';
import {Indicators} from './types/interfaces';
import {LoadingService} from '../../shared/services/loading.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  public dataAdvantage: AdvantageElement[];
  public role: string = 'client';
  public mainDataAdvantage: AdvantageElement[];
  public indicators: Indicators[];
  @ViewChild('homeId', {static: false}) home: ElementRef;


  constructor(private clientService: ClientService,
              private dataService: DataService,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    this.loadingService.setLoading(true);
    try {
      const data = await this.clientService.getAdvantage(this.role);
      const mainData = await this.clientService.getMainAdvantage(this.role);
      const response = await this.dataService.getIndicators(this.role);
      this.indicators = response.indicators;
      this.dataAdvantage = data.advantage;
      this.mainDataAdvantage = mainData.advantage;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
    this.home.nativeElement.scrollIntoView();
  }
}
