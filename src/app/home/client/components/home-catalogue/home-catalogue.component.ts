import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnInit,
  ViewChild
} from '@angular/core';
import {Lawyer, LawyerCardOptions} from "../../../../catalog/types/interfaces";
import {CdkScrollable, ScrollDispatcher} from "@angular/cdk/scrolling";
import {fadeInOnEnterAnimation, fadeOutOnLeaveAnimation} from "angular-animations";

@Component({
  selector: 'app-home-catalogue',
  templateUrl: './home-catalogue.component.html',
  styleUrls: ['./home-catalogue.component.scss'],
  animations: [
    fadeInOnEnterAnimation({delay: 300}),
    fadeOutOnLeaveAnimation({duration: 200})
  ]
})
export class HomeCatalogueComponent implements OnInit, AfterViewInit {
  @ViewChild('lawyerNav', {static: false}) public lawyerNav: ElementRef;
  public scrollPoint = 450;
  @Input() lawyer: Lawyer = {
    cellphone: "319205702",
    city: "Bogotá",
    // description: "",
    email: "abogado@gmail.com",
    experience: 10,
    id: "3",
    image: "/assets/svg/catalogue-lawyers/photo4.svg",
    name: "Johanna Alejandra Pérez Carrillo",
    office: "Oficina 304",
    phone: "2923746",
    score: 4,
    webPage: "dev.juridigo.com"

  };
  public lawyerOptions: LawyerCardOptions = {
    avatar: true,
    background: 'white-bg',
    contactOptions: true,
    description: false,
    office: true,
    premiumMark: false,
    hideInformation: false
  };
  currentPosition: any;
  startPosition: number;


  constructor(private scrollDispatcher: ScrollDispatcher,
              private ngZone: NgZone, private changes: ChangeDetectorRef) {

  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.scrollAnimation();
  }

  /**
   * allows to hide and show the lawyer information
   */
  public scrollAnimation() {
    this.scrollDispatcher.scrolled().subscribe(event => {
      if (event instanceof CdkScrollable) {
        this.lawyerOptions.hideInformation = event.getElementRef().nativeElement.scrollTop !== 0;
        if (event.getElementRef().nativeElement.scrollTop === 0) {
          this.changes.detectChanges();
        }
      }
    });
  }
}
