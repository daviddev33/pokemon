import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

// Services
import {ClientService} from '../../services/client.service';

// Interfaces
import {Card} from '../../types/interfaces';

// Components
import {LegalCardComponent} from '../legal-card/legal-card.component';
import {LoadingService} from "../../../../shared/services/loading.service";

@Component({
  selector: 'app-legal-services',
  templateUrl: './legal-services.component.html',
  styleUrls: ['./legal-services.component.scss']
})
export class LegalServicesComponent implements OnInit {
  public data: Card[];
  public buttonPlus: string;

  constructor(private clientService: ClientService,
              public dialog: MatDialog,
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    this.loadingService.setLoading(true);
   try {
       const data = await this.clientService.getLegalCard();
      this.data = data.card;
      this.buttonPlus = data.button;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  public openDialog(id) {
    this.dialog.open(LegalCardComponent, {
      data: {id},
      width: '90%',
      height: '90%',
      panelClass: 'custom-modal-box',
      backdropClass: 'styles-overlay'
    });
  }
}


