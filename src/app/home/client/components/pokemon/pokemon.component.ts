import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../types/interfaces/pokemon';
import { PokemonService } from '../../services/pokemon.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {
  public pokemons:Pokemon[]=[];

  constructor(private pokemonServices:PokemonService) { }

  ngOnInit(): void {
    this.getAllPokemons();
  }

  getAllPokemons(){
    this.pokemonServices.getAllPokemons()
    .subscribe(
      res=>{
        this.pokemons=res;        
      },
      err=>{
        console.log(err);
      }
    
    )
  }

}
