import {Component, OnInit, Inject, HostListener, ViewChild, ElementRef, ViewChildren, QueryList} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import SwiperCore, {Pagination, Navigation} from 'swiper/core';

// Interfaces
import {Card} from '../../types/interfaces';

// Services
import {ClientService} from '../../services/client.service';
import {LoadingService} from "../../../../shared/services/loading.service";

// this is use to pagination of swipe of advantage card.
SwiperCore.use([Pagination, Navigation]);

@Component({
  selector: 'app-legal-card',
  templateUrl: './legal-card.component.html',
  styleUrls: ['./legal-card.component.scss']
})
export class LegalCardComponent implements OnInit {
  public dataCard: Card[];
  public scrolled = true;

  @ViewChildren("itemsContainer")
  private itemsContainers: QueryList<ElementRef<HTMLElement>>;

  @ViewChildren("imagesContainer")
  private imagesContainer: QueryList<ElementRef<HTMLElement>>;

  public customStyles: Array<{ minHeight: string, height: string, alignContent: string }> = [];
  public flag: boolean = false;

  constructor(private clientService: ClientService,
              @Inject(MAT_DIALOG_DATA) public data: { id: string },
              private loadingService: LoadingService) {
  }

  async ngOnInit() {
    this.loadingService.setLoading(true);
   try {
       const data = await this.clientService.getLegalCardSelected();
      this.dataCard = data.card;
      for (const data of this.dataCard) {
        this.customStyles.push({minHeight: "100px", height: "unset", alignContent: "unset"});
        /*data.issue = [...data.issue, ...data.issue];*/
      }
      setTimeout(() => {
        this.calculateHeight();
      }, 1);
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.calculateHeight();
  }

  /**
   * calculates de height for items container to keep always two columns
   */
  calculateHeight() {
    for (let n = 0; n < this.itemsContainers.length; n++) {
      //
      const itemsContainer = this.itemsContainers.get(n).nativeElement;
      const imagesContainer = window.getComputedStyle(this.imagesContainer.get(n).nativeElement);
      // first set min height
      this.customStyles[n].minHeight = `calc(100% - ${imagesContainer.height} - ${imagesContainer.marginTop} - ${imagesContainer.marginBottom})`;
      // calculate items total height
      let totalHeight = 0;
      let sumFirstCalculatedHalf = 0;
      for (let i = 0; i < itemsContainer.children.length; i++) {
        const child = window.getComputedStyle(itemsContainer.children[i]);
        const calculatedValue = parseFloat(child.height.replace("px", "")) + parseFloat(child.marginTop.replace("px", "")) * 2;
        if (i < Math.floor(itemsContainer.children.length / 2)) {
          sumFirstCalculatedHalf += calculatedValue;
        }
        totalHeight += calculatedValue;
      }
      // split on two for two columns, add extra 10% to avoid box compensation and overflows
      let calculatedValue = Math.floor(totalHeight / 2);
      if (sumFirstCalculatedHalf > calculatedValue) {
        calculatedValue = sumFirstCalculatedHalf;
      }
      this.customStyles[n].height = calculatedValue + "px";
      this.customStyles[n].alignContent = `center`

    }
  }
}
