import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

// Services
import { DataService } from '../../services/data.service';
import { AlertsService } from '@services/alerts/alerts.service';

// Components
import { DialogComponent } from '../../../../utils/dialog/dialog.component';

// Utils
import { MatStepper } from '@angular/material/stepper';

// Interfaces
import { BranchesOfLaw, Cities} from '../../types/interfaces';
import {listDocuments} from '../../types/interfaces/type-document';
import {LoadingService} from "../../../../shared/services/loading.service";

@Component({
  selector: 'app-search-lawyer',
  templateUrl: './search-lawyer.component.html',
  styleUrls: ['./search-lawyer.component.scss']
})
export class SearchLawyerComponent implements OnInit {
  firstFormGroup: FormGroup; // Form Stepper One
  secondFormGroup: FormGroup; // Form Stepper Two
  searchForm: FormGroup; // Form Search Lawyer
  isEditable = true; // Edit Fields Stepper
  isChange = false; // Change View
  services: BranchesOfLaw[]; // Data BranchesOfLaw
  cities: Cities[]; // Data Cities
  private regexEmail = '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$'; // Pattern Email
  public documentTP = [
    {value: listDocuments.CC, viewValue: 'CC'},
    {value: listDocuments.CE, viewValue: 'CE'},
    {value: listDocuments.TI, viewValue: 'TI'},
    {value: listDocuments.RC, viewValue: 'RC'},
  ];

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private toast: AlertsService,
    private dialog: MatDialog,
    private loadingService: LoadingService
    ){
    this.buildForm();
  }

  async ngOnInit() {
     this.loadingService.setLoading(true);
   try {
       const responseServices = await this.dataService.getBranchesOfLaw();
      this.services = responseServices.services;
      const responseCities = await this.dataService.getCities();
      this.cities = responseCities.cities;
    } catch (error) {
      console.error(error);
      this.toast.showError(error.status, 'Error!');
    }
    this.loadingService.setLoading(false);
  }
  private buildForm() {
    this.firstFormGroup = this.formBuilder.group({
      case: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
    this.secondFormGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      documentType: ['', [Validators.required]],
      document: ['', [Validators.required, Validators.pattern(/[0-9+\- ]/)]],
      phone: ['', [Validators.required, Validators.minLength(7), Validators.pattern(/[0-9+\- ]/)]],
      email: ['', [Validators.required, Validators.pattern(this.regexEmail)]]
    });
    this.searchForm = this.formBuilder.group({
      service: ['', [Validators.required]],
      city: ['', [Validators.required]]
    });
  }
  /**
   * @public Change View Component to Steppers Forms
   * @return void
   */
  public changeComponent() {
    this.isChange = !this.isChange;
  }

  /**
   * @public Save two Froms Steppers and Valid Errors
   * @return void
   */
  public saveForm(firstForm, secondForm, stepper: MatStepper) {
    const nameField = this.secondFormGroup.get('name');
    const phoneField = this.secondFormGroup.get('phone');
    const emailField = this.secondFormGroup.get('email');
    const documentTypeField = this.secondFormGroup.get('documentType');
    const documentField = this.secondFormGroup.get('document');
    if (this.firstFormGroup.valid && this.secondFormGroup.valid) {
      stepper.reset();
      this.dialog.open(DialogComponent, {
        panelClass: 'dialog-success'
      });
    } else if (nameField.invalid && phoneField.invalid && documentField.invalid && documentTypeField.invalid && emailField.invalid){
      this.secondFormGroup.markAllAsTouched();
      this. toast.showError('Ingrese todos los Campos Requeridos', '');
    } else if (nameField.invalid) {
      this.toast.showWarning('Ingresa tu Nombre Completo', '');
    } else if (documentTypeField.invalid) {
      this.toast.showWarning('Ingrsa el tipo de Documento', '');
    } else if (documentField.invalid) {
      this.toast.showWarning('Ingresa tu Número de Documento', '');
    } else if (phoneField.invalid) {
      this.toast.showWarning('Ingresa tu Número Celular', '');
    } else if (emailField.invalid) {
      this.toast.showWarning('Ingresa tu Email', '');
    }
  }

  public nextStepper(stepper: MatStepper) {
    const caseField = this.firstFormGroup.get('case');
    const descriptionField = this.firstFormGroup.get('description');
    if (caseField.invalid && descriptionField.invalid) {
      this.toast.showError('Ingrese todos los Campos Requeridos', '');
      this.firstFormGroup.markAllAsTouched();
    } else if (this.invalidFieldsOne('case')){
      this.toast.showWarning('Ingresa el Asunto', '');
    } else if (this.invalidFieldsOne('description')) {
      this.toast.showWarning('Ingresa una breve Descripción del Asunto', '');
    } else {
      stepper.next();
    }
  }
  /**
   * @public Functions Getters FormControls
   * @return Methods Touched and Invalid Fields
   */
  public invalidFieldsOne(field: string) {
    const fieldName = this.firstFormGroup.get(field);
    return fieldName.touched && fieldName.invalid;
  }

  public invalidFieldsTwo(field: string) {
    const fieldName = this.secondFormGroup.get(field);
    return fieldName.touched && fieldName.invalid;
  }

  public invalidSearchForm(field: string) {
    const fieldName = this.searchForm.get(field);
    return fieldName.touched && fieldName.invalid;
  }

  /**
   * @public Search Lawyer with Filter Selects
   * @return New Page with More Lawyers Filters
   */
  startSearchLawyer() {
    if (this.searchForm.invalid) {
      this.searchForm.markAllAsTouched();
      this.toast.showError('Seleccione ambos campos', '');
      return;
    }
    const service = this.searchForm.get('service').value.toLowerCase();
    const city = this.searchForm.get('city').value.toLowerCase();
    this.searchForm.reset();
  }
}
