interface ArrayBranchesOfLaw{
    services: BranchesOfLaw[];
}

interface BranchesOfLaw {
    name: string;
}
export {
  ArrayBranchesOfLaw,
  BranchesOfLaw,
};
