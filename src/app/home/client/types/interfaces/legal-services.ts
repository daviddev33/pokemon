interface LegalServices {
  button?: string;
  card: Card[];
}

interface Card {
  id: string;
  icon: string;
  title: string;
  background?: string;
  issue?: string[];
}

export {
  LegalServices,
  Card
};
