import { Actions } from "app/utils/types/interfaces";
import { User } from "./index";

export interface Comments {
    id:number;
    user: User;
    description: string;
    createdAt: string;
    answers?: Comments[];
    actions?: Actions;
}

