import { Actions } from "app/utils/types/interfaces/index";
import { Comments } from "./index";


interface ArrayLastNews {
    news: LastNews[];
}

interface LastNews {
    id: string;
    img: string;
    title: string;
    description: string;
    photoProfile: string;
    author: string;
    createdAt: string;
    subtitles: string;
    actions: Actions;
    comments: Comments[];
}

export {
  ArrayLastNews,
  LastNews
};
