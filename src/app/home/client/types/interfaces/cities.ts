interface ArrayCities {
    cities: Cities[];
}

interface Cities {
    name: string;
}

export {
  ArrayCities,
  Cities
};
