interface IndicatorsArray {
    indicators: Indicators[];
}

interface Indicators {
    img: string;
    name: string;
    count: string;
}

export {
  Indicators,
  IndicatorsArray
};
