import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';

// Components
import {ClientComponent} from './client.component';
import {HomeCatalogueComponent} from './components/home-catalogue/home-catalogue.component';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent
  },
  {
    path: 'perfil-profesional',
    component: HomeCatalogueComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RouterModule
  ],
  exports: [
    RouterModule
  ]
})
export class ClientRoutingModule {
}
