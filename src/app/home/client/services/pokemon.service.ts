import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FetchAllPokemonResponse, Pokemon } from '../types/interfaces/pokemon';
import {map} from "rxjs/operators";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private url="https://pokeapi.co/api/v2/pokemon/?limit=1500";

  constructor(private http:HttpClient) { }

  getAllPokemons():Observable<Pokemon[]>{
    return this.http.get<FetchAllPokemonResponse>(this.url)
    .pipe( 
      map(this.transformSmallPokemonIntoPokemon)   
    );
  }
  private transformSmallPokemonIntoPokemon(resp:FetchAllPokemonResponse):Pokemon[]{
    
    const pokemonList:Pokemon[] = resp.results.map(poke=>{
      const urlArreglo=poke.url.split('/');
      const id=urlArreglo[6];
      const pic = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+id+".png";      
      return {
        id,
        pic,
        name:poke.name,        
      }
    })
    return pokemonList;
  }
}
