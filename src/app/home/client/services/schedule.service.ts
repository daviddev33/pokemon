import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Lawyer} from '../../../catalog/types/interfaces';

@Injectable({providedIn: 'root'})
export class ScheduleService {
  constructor(private http: HttpClient) {
  }

  public getSchedule() {
    return this.http.get<Lawyer>('../../assets/data/hours-date.json').toPromise();
  }
}
