import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// Interfaces
import {
  ArrayLastNews,
  ArrayBranchesOfLaw,
  IndicatorsArray,
  ArrayCities,
  LastNews,
} from '../types/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  public getBranchesOfLaw() {
    return this.http.get<ArrayBranchesOfLaw>('../../../assets/data/services.json').toPromise();
  }

  public getCities() {
    return this.http.get<ArrayCities>('../../../assets/data/cities.json').toPromise();
  }

  public getIndicators(flag: string) {
    if (flag === 'client') {
      return this.http.get<IndicatorsArray>('../../../assets/data/indicators.json').toPromise();
    } else if(flag === 'lawyer') {
      return this.http.get<IndicatorsArray>('../../../assets/data/indicators-lawyer.json').toPromise();
    }
  }

  public lastNews() {
    return this.http.get<ArrayLastNews>('../../../assets/data/last-news.json').toPromise();
  }

  public getBlog(id) {
    return this.http.get<LastNews>('../../../../assets/data/last-news.json').toPromise();
  }
}
