import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

// Interfaces
import {Advantage, LegalServices} from '../types/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  constructor( private http: HttpClient ) {}
  public getAdvantage(flag: string){
    if (flag === 'client') {
      return this.http.get<Advantage>('../../assets/data/advantage.json').toPromise();
    } else if (flag === 'lawyer') {
      return this.http.get<Advantage>('../../assets/data/advantage-lawyer.json').toPromise();
    }
  }
  public getMainAdvantage(flag: string) {
    if (flag === 'client') {
      return this.http.get<Advantage>('../../assets/data/main-advantage.json').toPromise();
    } else if (flag === 'lawyer') {
      return this.http.get<Advantage>('../../assets/data/main-advantage-lawyer.json').toPromise();
    }
  }
  public getLegalCard(){
    return this.http.get<LegalServices>('../../assets/data/catalogue.json').toPromise();
  }
  public getLegalCardSelected(){
    return this.http.get<LegalServices>('../../assets/data/catalogue-level-2.json').toPromise();
  }
}
