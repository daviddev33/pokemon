import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Modules
import { MaterialDesignerModule } from 'app/material-designer/material-designer.module';
import { ClientModule } from './client/client.module';
import { RouterModule } from '@angular/router';
import { LawyerModule } from './lawyer/lawyer.module';
import { HomeRoutingModule } from './home-routing.module';
import {SharedModule} from '../shared/shared.module';
import { UtilsModule } from 'app/utils/utils.module';


// Components
import {HomeComponent} from './home.component';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    MaterialDesignerModule,
    RouterModule,
    LawyerModule,
    SharedModule,
    ClientModule,
    UtilsModule,
    HomeRoutingModule,
  ],
  exports: [
    HomeComponent,
  ],
})
export class HomeModule {
}
