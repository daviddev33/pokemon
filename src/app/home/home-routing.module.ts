import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// components
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: 'inicio/cliente',
    component: HomeComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import ('../home/client/client.module').then(m => m.ClientModule),
        },
        {
          path: 'autenticacion',
          loadChildren: () => import ('../auth/auth.module').then(m => m.AuthModule)
        },
        {
          path: 'blogs',
          loadChildren: () => import ('../blog/blog.module').then(m => m.BlogModule)
        },
        {
          path: 'catalogo',
          loadChildren: () => import ('../catalog/catalog.module').then(m => m.CatalogModule)
        }
    ]
  },
  {
    path: 'inicio/abogado',
    component: HomeComponent,
    children:
    [
      {
        path: '',
        loadChildren: () => import ('../home/lawyer/lawyer.module').then(m => m.LawyerModule),
      },
      {
        path: 'autenticacion',
        loadChildren: () => import ('../auth/auth.module').then(m => m.AuthModule)
      },
      {
        path: 'blogs',
        loadChildren: () => import ('../blog/blog.module').then(m => m.BlogModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'top'})],
})
export class HomeRoutingModule { }
