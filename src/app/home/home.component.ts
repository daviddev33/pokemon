import {Component, NgZone, OnInit} from '@angular/core';
import {CdkScrollable, ScrollDispatcher} from '@angular/cdk/scrolling';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private readonly SHRINK_TOP_SCROLL_POSITION = 2;
  public shrinkToolbar = false;

  constructor(
    private scrollDispatcher: ScrollDispatcher,
    private ngZone: NgZone,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit()
    :
    void {
    this.scrollDispatcher.scrolled()
      .pipe(map((event: CdkScrollable) => this.getScrollPosition(event)))
      .subscribe(scrollTop => this.ngZone.run(() => this.shrinkToolbar = scrollTop > this.SHRINK_TOP_SCROLL_POSITION));


  }


  getScrollPosition(event) {
    if (event) {
      return event.getElementRef().nativeElement.scrollTop;
    } else {
      return window.scrollY;
    }
  }

  toServices() {
    document.getElementById('services').scrollIntoView();
  }

  /**
   * Returns true if is lawyer
   */
  get isLawyer() {
    return this.router.url.includes('inicio/abogado');
  }

  /**
   * Returns lawyer | client
   */
  get user() {
    if (this.isLawyer) {
      return 'abogado';
    } else {
      return 'cliente';
    }
  }

}
