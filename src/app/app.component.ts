import {Component} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

// Interfaces
import {IconSVG} from './types';
import {LoadingService} from './shared/services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front-JuridiGo';
  private lastVH = -1;
  private lastVW = -1;

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              public loadingService: LoadingService) {
    // Icons
    this.iconsAdvantageClient();

  }

  ngOnInit(): void {
    /**
     *capture the size of the viewport height
     *calculate the equivalence to 1vh and
     *save the value creating a css variable
     */
    const calculateViewport = () => {
        // We execute the same script as before
        let vh = window.innerHeight * 0.01;
        let vw = window.innerWidth * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
        document.documentElement.style.setProperty('--vw', `${vw}px`);
        return {vh, vw};
      };
    window.addEventListener('resize', calculateViewport);
    let eventFired = false;
    window.addEventListener('orientationchange', () => {
      if(eventFired){ return; }
      eventFired = true;
      let running = true;
      for(const delay of [50, 100, 200, 300, 500, 800, 1000]){
        setTimeout(() => {
          if(running){
            const {vh,vw} = calculateViewport();
            console.log("[orientationchange " + delay + "] vh:", vh, "this.lastVH", this.lastVH, ",vw:", vw, "this.lastVW", this.lastVW);
            if(vh !== this.lastVH || vw !== this.lastVW){
              this.lastVH = vh;
              this.lastVW = vw;
              running = false;
            }
          }
        }, delay);
      }
      eventFired = false;
    });
    const {vh,vw} = calculateViewport();
    this.lastVH = vh;
    this.lastVW = vw;
    console.log("[initialVH] ", this.lastVH);
    console.log("[initialVW] ", this.lastVW);
    //
  }

  public iconsAdvantageClient() {
    const icons: IconSVG[] = [
      {name: 'SaveTime@advantageClient', route: '../assets/svg/advantage/ahorra_tiempo.svg'},
      {name: 'twitter@footer', route: '../assets/svg/footer/twitter activo.svg'}
    ];
    for (const icon of icons) {
      this.registerIcon(icon);
      this.iconsLike();
    }
  }

  public iconsLike() {
    const icons: IconSVG[] = [
      {name: 'Like@blog', route: '../assets/svg/icons/like.svg'},
      {name: 'Dislike@blog', route: '../assets/svg/icons/dislike.svg'},
    ];
    for (const icon of icons) {
      this.registerIcon(icon);
    }
  }

  private registerIcon(icon: IconSVG) {
    const {name, route} = icon;
    this.matIconRegistry.addSvgIcon(name,
      this.domSanitizer.bypassSecurityTrustResourceUrl(route)
    );
  }

}
