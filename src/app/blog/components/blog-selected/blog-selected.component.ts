import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

// Services
import {DataService} from 'app/home/client/services/data.service';
import {Actions} from 'app/utils/types/interfaces';

// Interfaces
import {Comments, LastNews} from '../../../home/client/types/interfaces';
import * as moment from "moment";
import {LoadingService} from "../../../shared/services/loading.service";

@Component({
  selector: 'app-blog-selected',
  templateUrl: './blog-selected.component.html',
  styleUrls: ['./blog-selected.component.scss']
})
export class BlogSelectedComponent implements OnInit {

  private textAreaForm: FormGroup;
  public blogInfoArr: LastNews[] = [];
  public comments: Comments[] = [];
  public actionsComment: Actions;
  public blogId: string;
  public dateBlog: string;
  public isDisabled = true;

  constructor(
    private dataService: DataService,
    private activeRouter: ActivatedRoute,
    private formBuilder: FormBuilder,
    private loadingService: LoadingService) {
    this.buildForm();
  }

  async ngOnInit() {
    await this.getSelectedBlogInfo();
    this.getComment(this.blogInfoArr);
    this.getLikes(this.comments);
  }

  buildForm() {
    this.textAreaForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.maxLength(500)]]
    })
  }

  /**
   * gets the list of all blogs and the ID of the blog selected,
   * whit this data invokes the getBlogsInfo method
   */
  public async getSelectedBlogInfo() {
    this.loadingService.setLoading(true);
    try {
      const response = await this.dataService.lastNews();
      this.blogId = this.activeRouter.snapshot.paramMap.get('id');
      this.blogInfoArr = this.getBlogsInfo(response.news, this.blogId)
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  /**
   * receives an array with all blogs and the ID of the selected blog,
   * filters the list and return the blog that matches the id
   * @param lastNewsArr
   * @param id
   */
  public getBlogsInfo(lastNewsArr, id) {
    return lastNewsArr.filter(blog => {
      return blog.id === id
    });
  }

  /**
   * receives an array with the filtered blog as the only element,
   * maps it for comments and assigns them to the this.comments array
   * @param blogInfoArr
   */
  public getComment(blogInfoArr: LastNews[]) {
    return blogInfoArr.map(obj => {
      this.dateBlog = this.formatDate(obj.createdAt);
      if (!obj.comments) {
        return;
      }
      obj.comments.forEach(comment => {
        this.comments.unshift(comment);
      });
    });
  }

  /**
   * receives an array of comments and extracts the actions property
   * which is an object with the like and dislike properties and assigns it
   * to the actionsComment variable
   * @param comments
   */
  public getLikes(comments: Comments[]) {
    comments.forEach(comment => {
      if (!comment.actions) {
        return;
      }
      this.actionsComment = comment.actions;
    })
  }

  public formatDate(date: string) {
    return moment(date).format('DD MMMM YYYY');
  }

}
