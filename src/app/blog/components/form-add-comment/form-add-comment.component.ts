import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-add-comment',
  templateUrl: './form-add-comment.component.html',
  styleUrls: ['./form-add-comment.component.scss']
})
export class FormAddCommentComponent implements OnInit {

  public textAreaForm: FormGroup;
  public isDisabled:boolean = true;
  @Output() emitterCloseForm = new EventEmitter<boolean>();

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  private buildForm() {
    this.textAreaForm = this.formBuilder.group({
      comment:['', [Validators.required, Validators.maxLength(500)]]
    })
  }

  public writingComment(value: boolean){
    this.isDisabled = value;
   }

   public cancel(value: boolean){
     this.isDisabled = value;
     this.emitterCloseForm.emit(false);
     this.textAreaForm.reset()
   }

   public sendComment(){
     const comment = this.textAreaForm.get('comment').value;
     this.emitterCloseForm.emit(false);
     this.textAreaForm.reset()
   }

}
