import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';

// Services
import {DataService} from '../../../home/client/services/data.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-blog-main',
  templateUrl: './blog-main.component.html',
  styleUrls: ['./blog-main.component.scss'],
})
export class BlogMainComponent implements OnInit {
  public changes = new Subject<void>();
  public branchesOfLaw = [];
  public lastNews = [];
  public pageSize: number;
  public pageNumber = 1;
  public sizeScreen: number;

  constructor(private dataService: DataService,
              private router: Router,
              private activatedRouter: ActivatedRoute,
              private loadingService: LoadingService
  ) {
  }

  async ngOnInit() {
    this.sizeScreen = window.innerWidth;
    this.pageSize = this.numberPageSize;
    window.addEventListener('resize', () => {
      this.sizeScreen = window.innerWidth;
      this.pageSize = this.numberPageSize;
    })
    this.loadingService.setLoading(true);
    try {
      await this.getAllBranchesOfLaw();
      await this.getLastNews();
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  public async getAllBranchesOfLaw() {
    this.loadingService.setLoading(true);
    try {
      const response = await this.dataService.getBranchesOfLaw();
      this.branchesOfLaw = response.services;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  public async getLastNews() {
    this.loadingService.setLoading(true);
    try {
      const response = await this.dataService.lastNews();
      this.lastNews = response.news;
    } catch (error) {
      console.error(error);
    }
    this.loadingService.setLoading(false);
  }

  public handlePage(event: number) {
    this.pageNumber = 0;
    this.pageNumber = event;
  }

  public async eventCard(event: any) {
    await this.router.navigate(['./', event], {relativeTo: this.activatedRouter});
  }

  public get numberPageSize() {
    return this.sizeScreen < 1024 ? 3 : 6;
  }


}
