import {Component, Input, OnInit} from '@angular/core';
import {DataService} from 'app/home/client/services/data.service';
import {BranchesOfLaw, LastNews} from 'app/home/client/types/interfaces';
import {LoadingService} from "../../../shared/services/loading.service";

@Component({
  selector: 'app-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.scss']
})
export class RecommendedComponent implements OnInit {

  @Input() blogId: string;
  public categories: BranchesOfLaw[];
  public otherBlogs: LastNews[] = [];
  public pageSize: number = 3;
  public pageNumber: number = 1;

  constructor(
    private dataService: DataService,
    private loadingService: LoadingService
  ) {
  }

  async ngOnInit(){
    await this.getCategories();
    await this.getOtherBlogs();
  }

  async getCategories() {
    this.loadingService.setLoading(true);
   try {
       const responseServices = await this.dataService.getBranchesOfLaw();
      this.categories = responseServices.services;
    } catch (err) {
      console.error(err);
    }
    this.loadingService.setLoading(false);
  }

  async getOtherBlogs() {
    this.loadingService.setLoading(true);
   try {
       const response = await this.dataService.lastNews();
      this.otherBlogs = response.news;
      // this.otherBlogs = response.news.filter(blog => blog.id != this.blogId);
    } catch (err) {
      console.error(err);
    }
    this.loadingService.setLoading(false);
  }

  public handlePage(value: number) {
    this.pageNumber = value;
  }


}
