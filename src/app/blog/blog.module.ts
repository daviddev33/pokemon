import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Router
import { BlogRoutingModule } from './blog-routing.module';

// Components

import { BlogComponent } from './blog.component';
import { BlogSelectedComponent } from './components/blog-selected/blog-selected.component';
import { BlogMainComponent } from './components/blog-main/blog-main.component';
import { FormAddCommentComponent } from './components/form-add-comment/form-add-comment.component';
import { RecommendedComponent } from './components/recommended/recommended.component';
import { CommentsComponent } from './components/comments/comments.component';

// Modules
import { MaterialDesignerModule } from 'app/material-designer/material-designer.module';
import { MatButtonModule } from '@angular/material/button';
import { UtilsModule } from 'app/utils/utils.module';
import { SharedModule } from 'app/shared/shared.module';
import {CommonComponentsModule} from '../home/common-components/common-components.module';

@NgModule({
  declarations: [
    BlogMainComponent,
    BlogComponent,
    BlogSelectedComponent,
    CommentsComponent,
    RecommendedComponent,
    FormAddCommentComponent,

  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    MaterialDesignerModule,
    MatButtonModule,
    UtilsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CommonComponentsModule
  ]
})
export class BlogModule { }
