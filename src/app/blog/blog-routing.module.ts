import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { BlogMainComponent } from './components/blog-main/blog-main.component';
import { BlogSelectedComponent } from './components/blog-selected/blog-selected.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: BlogMainComponent},
      {path: ':id', component: BlogSelectedComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
